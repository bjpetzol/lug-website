---
layout: default
---

About
-----
The Linux Users' Group at NC State is a student group dedicated to maker
culture and serving the free & open source software community. Founded in Fall
of 1994, we are one of the oldest existing LUGs. While we have Linux in the
name, our group is more like a general technology enthusiasts club.

Given the nature of the LUG, our membership mostly consists of software
developers and students from the NC State Computer Science Department, but we
don't turn anyone away - there are no dues, and you don't have to be a student
to join.

Membership
----------
If you are interested in joining the LUG, membership is easy to obtain. Simply
attend meetings and make yourself known to the club, and we will be happy to
add you to the roster. Please do not email us asking for membership or submit
a request through NC State's Get Involved system.

Feel free to [join us on Discord, Matrix, or IRC](contact) and introduce yourself!

Mailing List
------------
We have a [mailing list](contact/#mailing-list) hosted on Google Groups where we
send out official updates, announcements, polls, and other information.

Meetings
--------
The LUG meets every week during the Spring and Fall semesters, typically Friday
nights at 7:00 PM. From week to week, we alternate activities from tech talks to
hack days to the occasional social dinner. Technical sessions and hack days are
generally held in the same room over the course of the semester. Any room
changes will be sent out on the mailing list. Venues for social dinners are
decided upon the week prior to the meeting, and then sent out on the mailing
list.

Our meetings are open to the public. We welcome members and non-members alike.

**Meeting Location**

~~~
Engineering Bldg II, Room 3001
890 Oval Dr,
Raleigh, NC 27606
~~~

**Meeting Schedule**

See [Events](events)

**Parking**

We hold our meetings on NC State’s Centennial Campus. Parking at NC State is
free after 5 PM in most places (be sure to check the signs). When parking on
Centennial, the best place to get a spot is in the CC decks on Partners Way. If
there are spots open on Oval Drive that is also convenient. There are four
decks on Partners: two CC, one Public pay parking, and one private corporate
deck.

**Attendance**

Club attendance varies based on time of year, active member count, events,
speakers, etc. Presently we hover around 8-10 individuals per meeting.

Services
--------

**Shell Accounts**

We offer shell accounts to members. If you would like a shell account, you must
be a member.  To be a member you must come to meetings regularly. We do not
give out shell accounts to people we have not met, so don't bother asking :-)

**ZNC Bouncer**

If you really want to get the most out of IRC, a bouncer allows you to keep a
connection alive in your channels 24/7 so that you never miss a beat when
you're offline. We run a bouncer (ZNC) on the LUG Server, Short. **It is a
members-only service.** If you're a member and would like access to ZNC, drop
a line in the IRC channel.

Guest Speakers
--------------
The LUG is a close-knit community of hackers and makers who like to get
together on a weekly basis to geek out over technology and creativity. We
welcome guest speakers who share in this passion to come and speak with us, not
to us.

Topics the LUG is interested in include:

* programming
* computer software
* computer hardware
* computer ethics
* [hacking](https://en.wikipedia.org/wiki/Hacker_culture)
* free & open source movements
* and similar!

Advisor
-------
[Dr. Jamie Jennings, NCSU CSC Faculty](https://www.csc.ncsu.edu/people/jajenni3)

[discord]: https://discord.gg/kep9qRc7nR
[matrix]: https://matrix.org
[matrix-presentation]: https://gitlab.com/isharacomix/presentations/-/blob/master/matrix/README.md
