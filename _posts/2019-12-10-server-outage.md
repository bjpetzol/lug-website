---
layout: post
title:  Temporary upcoming server outage
date:   2019-12-10
---

Hey everyone, just wanted to let you know the club's servers will be down for
a few days over the upcoming break while the server room has a new ventilation
system installed. It should be down from 12/29 4 PM to 1/4 9 AM. Sorry for the
inconvenience.
