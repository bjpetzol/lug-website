// Port types as parameters
module and_dataflow(
	input wire a, b,
	output wire z
);

	// Dataflow
	assign z = a & b;

endmodule

// Port types as parameters
module and_behavioral(
	input wire a, b,
	output reg z
);

	// Behavioral
	always @( a or b ) begin
		if ( a == 1'b1 && b == 1'b1 ) begin
			z = 1'b1;
		end
		else begin
			z = 1'b0;
		end
	end

endmodule

module and_tb; // _tb = test bench

	reg a, b,  // AND inputs
	    check; // Bit toggled to trigger check
	wire z_dataflow, z_behavioral; // Outputs

	// Creates modules to test
	// Leaving gate-level because simulation requires libraries (which
	// makes it more complicated)
	// .<x>(<y>) specifies that <y> should be assigned to port <x>
	and_dataflow   and1( .a( a ), .b( b ), .z( z_dataflow ) );
	and_behavioral and2( .a( a ), .b( b ), .z( z_behavioral ) );

	// Shows outputs - manual checking
	// More sophisticated checking methods exist
	always @( check ) begin
		$display(
			"Time: %d  a: %b  b: %b  z df: %b  z bh: %b",
			$time, a, b, z_dataflow, z_behavioral
		);
	end

	// Changes values
	initial begin
		#1 a = 0; b = 0; check = 0;
		#1 a = 0; b = 1; check = ~check;
		#1 a = 1; b = 0; check = ~check;
		#1 a = 1; b = 1; check = ~check;
	end

endmodule

module and_tb_selfchecking;

	reg [1:0] inputs [3:0]; // 4 inputs, 2 bits each
	reg outputs [3:0];      // 4 outputs, 1 bit each
	reg a, b, clk, reset;   // clk and reset are to prevent sim issues
	wire z_dataflow, z_behavioral;
	integer i, errors; // index for inputs/outputs, # errors

	// Creates modules to test
	and_dataflow   and1( .a( a ), .b( b ), .z( z_dataflow ) );
	and_behavioral and2( .a( a ), .b( b ), .z( z_behavioral ) );

	// Pulses clock
	always begin #5; clk = ~clk; end

	// Initializes values
	initial begin
		inputs[0] = 2'b00; inputs[1] = 2'b01;
		inputs[2] = 2'b10; inputs[3] = 2'b11;
		outputs[0] = 1'b0; outputs[1] = 1'b0;
		outputs[2] = 1'b0; outputs[3] = 1'b1;
		errors = 0; i = 0; clk = 0;
		reset = 1; #1 reset = 0;

		$dumpfile( "and.vcd" );
		$dumpvars; // write all vars
		// CVC-specific:
		//$fstDumpfile( "and.vcd" );
		//$fstDumpvars; // write all vars
	end

	// Assigns inputs; posedge means to only do it on the rising edge
	// (only when it changes from 0 to 1)
	always @( posedge clk ) begin
		#1 { a, b } = inputs[i]; // 'Unpacks' inputs[i] into a and b
	end

	always @( negedge clk ) begin
		if ( ~reset ) begin
			// !==: Special kind of compare; doesn't ignore X's
			// (don't cares)
			if ( z_dataflow !== outputs[i] ) begin
				$display(
					"dataflow   %d failed: %d vs %d",
					i, z_dataflow, outputs[i]
				);
				errors = errors + 1;
			end
			if ( z_behavioral !== outputs[i] ) begin
				$display(
					"behavioral %d failed: %d vs %d",
					i, z_behavioral, outputs[i]
				);
				errors = errors + 1;
			end

			// Unless we have a test input of don't cares (which
			// would be pretty unusual), this is a fine way to see
			// if we're at the end
			i = i + 1;
			if ( inputs[i] === 2'bx ) begin
				$display( "Test completed with %d errors", errors );
				$finish;
			end
		end
	end

endmodule
