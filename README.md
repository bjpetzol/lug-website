lug-website
===========
Website for the Linux User Group @ NC State.

Building
--------
Depends: `ruby >= 2.3.1`.

```
$ gem install jekyll jekyll-paginate jekyll-seo-tag jekyll-mentions jemoji webrick
$ git clone https://gitlab.com/ncsulug/lug-website.git
$ cd lug-website
$ jekyll build
```

The generated site will be placed in `_site`.

Testing
---
To test the site locally, run `jekyll serve --baseurl $(pwd)`

Note that using this method, relative URLs will be incorrect.

TODO
---

* Figure out way to make all URLs correct with local testing

* Implement script to check links are correct

* Add non-archived emails (from inbox) to server somehow

(The following will have to be done on server; code is not in repo)

* Implement voting
